﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;

namespace EiS
{
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            SchemeImage1.Visibility = Visibility.Hidden;
            SchemeImage2.Visibility = Visibility.Hidden;
        }

        private void Button_OnClick(object sender, RoutedEventArgs e)
        {
            SchemeImage1.Visibility = Convert.ToDouble(C.Text) <= 0 ? Visibility.Visible : Visibility.Hidden;
            SchemeImage2.Visibility = Convert.ToDouble(C.Text) <= 0 ? Visibility.Hidden : Visibility.Visible;
            var wave = new Wave(Convert.ToDouble(F.Text), Convert.ToDouble(U.Text), Convert.ToDouble(C.Text), Convert.ToDouble(Rn.Text));
            var waveValues = new List<KeyValuePair<double, double>>();
            var rectifiedWaveValues = new List<KeyValuePair<double, double>>();
            var normWaveValues = new List<KeyValuePair<double, double>>();
            for (double i = 0; i < wave.T * 2.5; i += wave.T / 100)
            {
                waveValues.Add(new KeyValuePair<double, double>(i, wave.U(i)));
                normWaveValues.Add(new KeyValuePair<double, double>(i, wave.UNorm(i)));
                rectifiedWaveValues.Add(Convert.ToDouble(C.Text) > 0 
                  ? new KeyValuePair<double, double>(i + wave.T / 4, wave.URectified(i))
                  : new KeyValuePair<double, double>(i, wave.U(i)));
            }
            for (double i = 0; i < wave.T / 4; i += wave.T / 100)
                rectifiedWaveValues.Add(new KeyValuePair<double, double>(i, wave.U(i)));
            WaveSeries.DataContext = waveValues;
            RectifiedWaveSeries.DataContext = rectifiedWaveValues;
            NormWaveSeries.DataContext = normWaveValues;
            var aver = Math.Round(Computing.Average(wave.URectified, wave.T), 2); // это нужно, чтобы потом посчитать коэффициент пульсации
            UAvg.Text = aver.ToString(CultureInfo.CurrentCulture); // среднее значение 
            UmIn.Text = Math.Round(Convert.ToDouble(U.Text) * Math.Sqrt(2), 2).ToString(CultureInfo.CurrentCulture); // входное аплитудное значение напряжения 
            var uRectifiedFourier = Dft.Fourier(wave.T, wave.URectified);
            var uFourier = Dft.Fourier(wave.T, wave.U);
            var uNormFourier = Dft.Fourier(wave.T, wave.UNorm);
            var uRectifiedFourierValues = new List<KeyValuePair<double, double>>();
            var uFourierValues = new List<KeyValuePair<double, double>>();
            var uNormFourierValues = new List<KeyValuePair<double, double>>();
            var funcNull1Values = new List<KeyValuePair<double, double>>(); // черная линия для первого фурье
            var funcNull2Values = new List<KeyValuePair<double, double>>(); // для второго
            var funcNull3Values = new List<KeyValuePair<double, double>>(); // для третьего 
            for (var i = 0; i < 10; i++)
            {
                uFourierValues.Add(new KeyValuePair<double, double>(i / wave.T - 0.00001, 0));
                uNormFourierValues.Add(new KeyValuePair<double, double>(i / wave.T - 0.00001, 0));
                uRectifiedFourierValues.Add(new KeyValuePair<double, double>(i / wave.T - 0.00001, 0));

                uRectifiedFourierValues.Add(new KeyValuePair<double, double>(i / wave.T, uRectifiedFourier[i]));
                uFourierValues.Add(new KeyValuePair<double, double>(i / wave.T, uFourier[i]));
                uNormFourierValues.Add(new KeyValuePair<double, double>(i / wave.T, uNormFourier[i]));

                uRectifiedFourierValues.Add(new KeyValuePair<double, double>(i / wave.T + 0.00001, 0));
                uFourierValues.Add(new KeyValuePair<double, double>(i / wave.T + 0.00001, 0));
                uNormFourierValues.Add(new KeyValuePair<double, double>(i / wave.T + 0.00001, 0));
            }
            funcNull1Values.Add(new KeyValuePair<double, double>(0, 0));
            funcNull2Values.Add(new KeyValuePair<double, double>(0, 0));
            funcNull3Values.Add(new KeyValuePair<double, double>(0, 0));
            funcNull1Values.Add(new KeyValuePair<double, double>(9 / wave.T, 0));
            funcNull2Values.Add(new KeyValuePair<double, double>(9 / wave.T, 0));
            funcNull3Values.Add(new KeyValuePair<double, double>(9 / wave.T, 0));

            UmOut.Text = Math.Round(uRectifiedFourier[1], 2).ToString(CultureInfo.CurrentCulture); // выходное аплитудное значение напряжения
            E.Text = (Math.Round(uRectifiedFourier[1] / Convert.ToDouble(aver), 2) * 100).ToString(CultureInfo.CurrentCulture); // коэффициент пульсации
            UNormFourierSeries.DataContext = uNormFourierValues;
            UFourierSeries.DataContext = uFourierValues;
            URectifiedFourierSeries.DataContext = uRectifiedFourierValues;
            FuncNull1.DataContext = funcNull1Values;
            FuncNull2.DataContext = funcNull2Values;
            FuncNull3.DataContext = funcNull3Values;
        }
    }
}