﻿using System;
using System.Linq;
using System.Numerics;

namespace EiS
{
    internal static class Computing
    {
        internal delegate double WaveFunc(double d);

        private static double Integrate(WaveFunc func, double T) // интегрирование 
        {
            var h = 0.005; //Шаг интегрирования
            var d = 1d;
            while (d > 0.001) // Изменяющаяся точность интегрирования
            {
                d = 1d / 3 * Math.Abs(IntegralCalc(func, T, h) - IntegralCalc(func, T, h / 2));
                h /= 2;
            }
            return Math.Round(IntegralCalc(func, T, h / 2), 2);
        }

        private static double IntegralCalc(WaveFunc func, double T, double h) // метод интегрирования 
        {
            var f = 0d;
            var n = Math.Round(T / h);
            for (var i = 0; i < n - 1; i++)
            {
                f += (func(i * h + T) + func((i + 1) * h + T)) / 2;
            }
            return f * h;
        }

        internal static double Average(WaveFunc func, double T) // среднее значение
        {
            return Integrate(func, T) / T;
        }
    }

    internal class Wave
    {
        // ReSharper disable once InconsistentNaming
        private readonly double _u, _f, _c, _r;
        internal readonly double T;
        private double _t2 = double.NaN;

        public Wave(double f, double u, double c, double r)
        {
            _f = f;
            _u = u * Math.Sqrt(2);
            _c = c / 1000000;
            _r = r;
            T = 1d / _f;     //период 
        }

        internal double U(double t)
        {
            t %= T;
            return t < 0.5 * T ? Math.Sin(_f * t * 2 * Math.PI) * _u : 0;
        }

        internal double UNorm(double t)
        {
            t %= T;
            return t < T / 2 ? U(t) : -U(t - T / 2);
        }

        private double Uc(double t)
        {
            return _u * Math.Exp(-(t - 0.25 * t) / (_r * _c));
        }
        internal double URectified(double t)
        {
            if (Math.Abs(_c) < double.Epsilon) return U(t);
            if (double.IsNaN(_t2))
            {
                double d = _u / 1000, s = T / 1000; //delta, step
                for (double i = 0; i < T / 2; i += s)
                {
                    if (d < Math.Abs(Uc(i) - U(i))) continue;
                    _t2 = i;
                    break;
                }
            }
            t %= T;
            return Math.Max(Uc(t), U(t + T / 4));
        }
    }
    internal static class Dft
    {
        private static double[] Transform(double[] input)
        {
            var n = input.Length;
            var output = new Complex[n];
            var arg = -2 * Math.PI / n;
            for (var i = 0; i < n; i++)
            {
                for (var j = 0; j < n; j++)
                {
                    output[i] += input[j] * Complex.FromPolarCoordinates(1, arg * i * j);
                }
            }
            return output.Select(x => x.Magnitude).ToArray();
        }

        internal static double[] Fourier(double T, Computing.WaveFunc wf)
        {
            const double samplingFreq = 120;
            const int numberValues = (int)samplingFreq;
            var waveArray = new double[numberValues];
            for (var i = 0; i < numberValues; i++)
            {
                var t = i / samplingFreq * T;
                waveArray[i] = wf(t);
            }
            var output = Transform(waveArray);
            var outputDoubles = output.Select(x => x > 1 ? x * 2 / samplingFreq : 0).ToArray();
            outputDoubles[0] /= 2;
            return outputDoubles;
        }
    }
}